var sysLangCode = 'mn';
var emailCorrect = false;

function onAanImgError (source) {
    source.src = "assets/custom/legal/image/ministry_icon/tme_thumb.jpg";
    source.onerror = "";
    return true;
}

function onZanImgError (source) {
    source.src = "assets/custom/legal/image/ministry_icon/zhhae_thumb.jpg";
    source.onerror = "";
    return true;
}

function onLawFileImgError28 (source) {
    source.src = "assets/custom/legal/image/icon/act-cover-28.jpg";
    source.onerror = "";
    return true;
}

function onLawFileImgError29 (source) {
    source.src = "assets/custom/legal/image/icon/act-cover-29.jpg";
    source.onerror = "";
    return true;
}

function onLawFileImgError30 (source) {
    source.src = "assets/custom/legal/image/icon/act-cover-30.jpg";
    source.onerror = "";
    return true;
}

function onLawFileImgError31 (source) {
    source.src = "assets/custom/legal/image/icon/act-cover-31.jpg";
    source.onerror = "";
    return true;
}

function onLawFileImgError32 (source) {
    source.src = "assets/custom/legal/image/icon/act-cover-32.jpg";
    source.onerror = "";
    return true;
}

function onLawFileImgError33 (source) {
    source.src = "assets/custom/legal/image/icon/act-cover-33.jpg";
    source.onerror = "";
    return true;
}

function onLawFileImgError34 (source) {
    source.src = "assets/custom/legal/image/icon/act-cover-34.jpg";
    source.onerror = "";
    return true;
}

function onLawFileImgError35 (source) {
    source.src = "assets/custom/legal/image/icon/act-cover-35.jpg";
    source.onerror = "";
    return true;
}

function onLawFileImgError36 (source) {
    source.src = "assets/custom/legal/image/icon/act-cover-36.jpg";
    source.onerror = "";
    return true;
}

function onLawFileImgError37 (source) {
    source.src = "assets/custom/legal/image/icon/act-cover-37.jpg";
    source.onerror = "";
    return true;
}

function onLawFileImgError38 (source) {
    source.src = "assets/custom/legal/image/icon/act-cover-38.jpg";
    source.onerror = "";
    return true;
}

function onLawFileImgError180 (source) {
    source.src = "assets/custom/legal/image/icon/act-cover-180.jpg";
    source.onerror = "";
    return true;
}

function onLawFileImgError186 (source) {
    source.src = "assets/custom/legal/image/icon/act-cover-186.jpg";
    source.onerror = "";
    return true;
}
function onLawFileImgError(source) {
    source.src = "assets/custom/legal/image/default-act.jpg";
    source.onerror = "";
    return true;
}

function onLawNewsImgError(source) {
    source.src = "assets/custom/legal/image/news.jpg";
    source.onerror = "";
    return true;
}

function onLawBookImgError(source) {
    source.src = "assets/custom/legal/image/nom1.png";
    source.onerror = "";
    return true;
}

function onCompilationImgError(source) {
    source.src = "assets/custom/legal/image/zhha_thumb.jpg";
    source.onerror = "";
    return true;
}

function login() {
    document.getElementById("loginBtn").onclick = function () {
        document.getElementById("loginform").submit();
    };
}

function pl() {
    $.ajax({
        type: 'POST',
        url: 'login/legalOauth',
        data: $('#loginform').serialize(),
        dataType: 'JSON',
        beforeSend: function () {
            Core.blockUI();
        },
        success: function (result) {
            if (result.status == 'success') {
                new PNotify({
                    title: 'Success',
                    text: result.text,
                    type: 'success',
                    sticker: false,
                });
                setTimeout(() => {
                    location.reload();
                }, 2000);
            } else {
                PNotify.removeAll();
                new PNotify({
                    title: 'Warning',
                    text: result.text,
                    type: 'warning',
                    sticker: false,
                });
            }
            Core.unblockUI();
        },
        error: function (err) {
            Core.unblockUI();
        }
    });
}

function resetPassword() {
    $('#resetPassForm').ajaxSubmit({
        type: 'POST',
        url: 'login/resetPassword',
        dataType: 'JSON',
        beforeSend: function () {
            Core.blockUI();
        },
        success: function (result) {
            if (result.status == 'success') {
                new PNotify({
                    title: 'Success',
                    text: result.text,
                    type: 'success',
                    sticker: false,
                });
                setTimeout(() => {
                    location.reload();
                }, 2000);
            } else {
                PNotify.removeAll();
                new PNotify({
                    title: 'Warning',
                    text: result.text,
                    type: 'warning',
                    sticker: false,
                });
            }
            Core.unblockUI();
        },
        error: function (err) {
            Core.unblockUI();
        }
    });
}

function pr() {

    var firstName = $('input[data-path="firstName"]').val();
    var lastName = $('input[data-path="lastName"]').val();
    var userName = $('input[data-path="userName"]').val();
    var email = $('input[data-path="customerEmail"]').val();
    var phoneNumber = $('input[data-path="phoneNumber"]').val();
    var fullAddress = $('input[data-path="fullAddress"]').val();
    var password = $('input[data-path="password"]').val();
    var password2 = $('input[data-path="password2"]').val();
    var reg1 = $('select[data-path="regFirstLetter"]').val();
    var reg2 = $('select[data-path="regSecondLetter"]').val();
    var regnum = $('input[data-path="regNum"]').val();
    var security_code = $('input[data-path="security_code"]').val();
    var stateRegNumber = '';
    PNotify.removeAll();

    if (reg1 != '' && reg1 != '' && regnum != '') {
        stateRegNumber = reg1 + reg2 + regnum;
    } else {
        new PNotify({
            title: 'Анхааруулга',
            text: 'Регистер буруу байна!',
            type: 'warning',
            sticker: false,
        });
    }

    if (password != password2) {
        new PNotify({
            title: 'Анхааруулга',
            text: 'Нууц үг таарахгүй байна!',
            type: 'warning',
            sticker: false,
        });
        return;
    }

    if (email == '') {
        new PNotify({
            title: 'Анхааруулга',
            text: 'Имэйл хаяг заавал бөглөнө үү!',
            type: 'warning',
            sticker: false,
        });
        return;
    }

    $.ajax({
        type: 'POST',
        url: URL_APP + URL_LANG + '/register',
        data: {
            firstName: firstName, 
            lastName: lastName, 
            username: userName, 
            password: password, 
            email: email, 
            stateRegNumber: stateRegNumber, 
            fullAddress: fullAddress, 
            phone: phoneNumber, 
            security_code: security_code
        },
        dataType: 'JSON',
        beforeSend: function () {
            Core.blockUI();
        },
        success: function (result) {
            if (result.status == 'success') {
                new PNotify({
                    title: 'Амжилттай',
                    text: 'Бүртгэл амжилттай хийгдлээ',
                    type: 'success',
                    sticker: false
                });
                $("div#register").removeClass("uk-flex uk-open");
                $("div#login").addClass("uk-open uk-flex");
            } else {
                new PNotify({
                    title: 'Алдаа',
                    text: result.text,
                    type: 'error',
                    sticker: false,
                });
            }

            Core.unblockUI();
        },
        error: function (err) {
            Core.unblockUI();
        }
    });
}

function new_captcha() {
    var c_currentTime = new Date();
    var c_miliseconds = c_currentTime.getTime();
    document.getElementById('captcha').src = 'api/captcha?x=' + c_miliseconds;
}

$(function () {
    
    $('.tw-chart-circle').each(function () {
        var $currentCircleChartCont = $(this);
        var $currentCircleChart = $('>.tw-chart', $currentCircleChartCont);
        $currentCircleChart.easyPieChart({
            animate: 1000,
            lineWidth: $currentCircleChart.attr('data-width'),
            size: $currentCircleChart.attr('data-size'),
            barColor: $currentCircleChart.attr('data-color'),
            trackColor: $currentCircleChart.attr('data-trackcolor'),
            scaleColor: false,
            lineCap: 'butt',
            onStep: function () {
                $currentCircleChart.css('line-height', $currentCircleChart.attr('data-size') + 'px');
                $currentCircleChart.css('width', $currentCircleChart.attr('data-size') + 'px');
                $currentCircleChart.css('height', $currentCircleChart.attr('data-size') + 'px');
            }
        });
        $currentCircleChartCont.on('inview', function () {
            $currentCircleChart.data('easyPieChart').disableAnimation().update(0).enableAnimation().update($currentCircleChart.attr('data-percent'));
        });
    });

    $('body').on('change', 'input[data-path="customerPhonenumber"]', function () {
        var $this = $(this);

        var testEmail = /^[0-9]{8}$/i;
        if (testEmail.test($this.val())) {
            registerCorrect = true;
        } else {
            registerCorrect = false;
            new PNotify({
                title: 'Анхааруулга',
                text: 'Утасны дугаар буруу байна!',
                type: 'warning',
                sticker: false,
            });
            $this.val('');
        }
    });


    $('body').on('click', '.font-small', function () {
        $(this).closest('body').removeClass('re-font-large');
        $(this).closest('body').removeClass('re-font-medium');
        $(this).closest('body').addClass('re-font-small');
    });

    $('body').on('click', '.font-medium', function () {
        $(this).closest('body').removeClass('re-font-large');
        $(this).closest('body').addClass('re-font-medium');
        $(this).closest('body').removeClass('re-font-small');
    });

    $('body').on('click', '.font-large', function () {
        $(this).closest('body').addClass('re-font-large');
        $(this).closest('body').removeClass('re-font-medium');
        $(this).closest('body').removeClass('re-font-small');
    });

});

$(document).ready(function () {
    setTimeout(() => {
        $('body').find('form').find('input[autocomplete="off"]').val('');
    }, 500);
    
});

function blockContent(mainSelector) {
    $(mainSelector).block({
        message: '<i class="icon-spinner4 spinner"></i>',
        centerX: 0,
        centerY: 0,
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            width: 16,
            top: '15px',
            left: '',
            right: '15px',
            border: 0,
            padding: 0,
            backgroundColor: 'transparent'
        }
    });
}


$('#loginform input').keypress(function (e) {
    
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 13) {
        pl();
        return false;
    }
});

$('body').on('click', 'a.login-form-modal', function () {
    UIkit.modal('#login').show();
    $('#login').find('input[name="user_name"]').focus();
});

$('body').on('click', 'a.register-form-modal', function () {
    UIkit.modal('#register').show();
    $('#register').find('input[id="lastName"]').focus();
});

$('body').on('click', '.navbar-toggler', function () {
    $('.toggle-menu-link').toggle();
});

$('body').on('change', '#register input[data-path="customerEmail"]', function () {
    var $this = $(this),
        $email = $this.val();

    var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    if (testEmail.test($email)) {
        emailCorrect = true;
    } else {
        emailCorrect = false;
        new PNotify({
            title: 'Анхааруулга',
            text: 'Имэйл хаяг буруу байна!',
            type: 'warning',
            sticker: false,
        });
        $this.val('');
    }

    if (emailCorrect) {
        $.ajax({
            type: 'POST',
            url: 'login/checkMail',
            data: { mail: $email },
            dataType: 'JSON',
            beforeSend: function () {
                Core.blockUI();
            },
            success: function (result) {
                if (result.status !== 'success') {
                    PNotify.removeAll();
                    new PNotify({
                        title: 'Warning',
                        text: result.text,
                        type: 'warning',
                        sticker: false,
                    });
                    $this.val('');
                }
                Core.unblockUI();
            },
            error: function (err) {
                Core.unblockUI();
            }
        });
    }
});

$('body').on('change', '#register input[data-path="userName"]', function () {
    var $this = $(this),
        $username = $this.val();
    $.ajax({
        type: 'POST',
        url: 'login/checkUsername',
        data: { user_name: $username },
        dataType: 'JSON',
        beforeSend: function () {
            Core.blockUI();
        },
        success: function (result) {
            if (result.status !== 'success') {
                PNotify.removeAll();
                new PNotify({
                    title: 'Warning',
                    text: result.text,
                    type: 'warning',
                    sticker: false,
                });
                /* $this.val(''); */
            }
            Core.unblockUI();
        },
        error: function (err) {
            Core.unblockUI();
        }
    });
});

$('body').on('change', '#register input[data-path="customerRegister"]', function () {
    var $this = $(this);

    var testEmail = /^[0-9]{8}$/i;
    if (testEmail.test($this.val())) {
        registerCorrect = true;
    } else {
        registerCorrect = false;
        new PNotify({
            title: 'Анхааруулга',
            text: 'Регистрийн дугаар буруу байна!',
            type: 'warning',
            sticker: false,
        });
        $this.val('');
    }
});