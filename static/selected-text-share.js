jQuery(document).ready(function () {
    try {
        if (!document.getElementById("share-wrapper")) {
            $('body').append('<div class="share-wrapper" id="share-wrapper" style="display:none;">\n\
                <div class="has_twitter" style="display: inline-block;">\n\
                <a href="javascript:;" class="selected-share-tw" onclick="shareTW();">\n\
                    <i class="fa fa-twitter"></i>\n\
                </a>\n\
            </div>\n\
            <div class="has_facebook" style="display: inline-block">\n\
                <a href="javascript:;" class="selected-share-fb" onclick="shareFB();">\n\
                    <i class="fa fa-facebook"></i>\n\
                </a>\n\
            </div>\n\
            <div class="has_facebook" style="display: inline-block">\n\
                <a href="javascript:;" class="selected-share-fb" onclick="listenSelector();">\n\
                    <i class="fa fa-hard-of-hearing"></i>\n\
                </a>\n\
            </div>\n\
        </div>');
        }

        if ($('body').find('#text-share-div').length > 0) {
            document.addEventListener('selectionchange', () => {
                var element = document.getElementById("text-share-div");
                element.onclick = getCursorXY;
            });
            function getCursorXY(e) {
                var x = document.getElementById("share-wrapper");
                var selection = getSelectionText();
                if (selection.length > 0) {
                    $('#share-wrapper').css("left", e.pageX - 40);
                    $('#share-wrapper').css("top", e.pageY + 30);
                    if (x.style.display == 'none') {
                        x.style.display = 'block';
                    } else {
                        x.style.display = 'none';
                    }
                } else {
                    x.style.display = 'none';
                }
            }
        }

    } catch (error) {
        console.log(error);
    }
    
});
function getSelectionText() {
    var text = "";
    if (window.getSelection) {
        text = window.getSelection().toString();
    } else if (document.selection && document.selection.type != "Control") {
        text = document.selection.createRange().text;
    }
    return text;
}
function shareTW() {
    var x = document.getElementById("share-wrapper");
    x.style.display = 'none';
    var tw_href = 'https://twitter.com/intent/tweet?via=legal_mn&url=%url%&text=%text%';
    var selection = getSelectionText();
    tw_href = tw_href.replace('%text%', selection);
    tw_href = tw_href.replace('%url%', window.location.href);
    var leftSize = (screen.width / 2) - 350;
    window.open(tw_href, "Share Twitter", "menubar=0,location=0,resizable=0,status=0,titlebar=0,toolbar=0,width=700,height==500,top=200,left=" + leftSize);


}
function shareFB(text) {
    var x = document.getElementById("share-wrapper");
    x.style.display = 'none';
    var fb_href = 'https://www.facebook.com/dialog/share?app_id=1060457520752909&display=popup&quote=%text%&href=%href%';
    var selection = (typeof text !== 'undefined') ? text : getSelectionText();
    fb_href = fb_href.replace('%text%', selection);
    fb_href = fb_href.replace('%href%', window.location.href);
    var leftSize = (screen.width - 700) / 2;
    window.open(fb_href, "Share Facebook", "menubar=0,location=0,resizable=0,status=0,titlebar=0,toolbar=0,width=700,height=500,top=200,left=" + leftSize);
}

function listenSelector(element) {
    $('body').find('.tw-fixed').hide();
    var $this = $(element),
        $parent = $this.closest('.listen'),
        text = $this.attr('data-text');

    $.ajax({
        type: 'post',
        url: URL_APP + URL_LANG + '/apichimege',
        data: { text: getSelectionText() },
        dataType: 'json',
        beforeSend: function () {
            Core.blockUI({
                message: 'Боловсруулж байна, түр хүлээнэ үү...',
                boxed: true
            });
        },
        success: function (response) {
            $('body').find('.tw-fixed .tw-fixed-content').empty().append(response.file).promise().done(function () {
                $('body').find('.tw-fixed').show();
                Core.unblockUI();
            });

        },
        error: function (jqXHR, exception) {
            Core.unblockUI();
            Core.showErrorMessage(jqXHR, exception);
        }
    });
}