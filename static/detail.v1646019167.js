
$.fn.slideFadeToggle = function (speed, easing, callback) {
    return this.animate({
        opacity: "toggle",
        height: "toggle"
    }, speed, easing, callback);
};

jQuery.fn.extend({
    scrollTo: function (speed, easing) {
        return this.each(function () {
            var targetOffset = $(this).offset().top - 50;
            $('html,body').animate({
                scrollTop: targetOffset
            }, speed, easing);
        });
    }
});

jQuery(document).ready(function () {

    $('input#high_val').on('keyup', function (e) {
        if (e.keyCode == 13) {
            if ($(this).val() != '') {
                if ($("#high_tmp").val() != $(this).val()) {
                    $("#high_tmp").val($(this).val());
                    $(".tab-pane").removeHighlight2();
                    $(".tab-pane.active").removeHighlight2().highlight2($(this).val());

                    $('.highlight2').each(function (index) {
                        $(this).attr("id", "anchor" + (index + 1));
                    });

                    if (parseInt($('.highlight2').length) >= 1) {
                        //$('#anchor1').closest('.msg_body').show().prev('.msg_head').addClass("opened_head");
                        $('#anchor1').scrollTo().animate({opacity: 0}, 'slow').animate({opacity: 1}, 'normal');
                        $("#anchor_num").val(2);
                    }
                    $(".law_highlight span.count").empty().append("Нийт олдсон: " + $('.highlight2').length);
                } else {
                    var anchor_num = parseInt($("#anchor_num").val());

                    $('#anchor' + anchor_num).closest('.msg_body').show().prev('.msg_head').addClass("opened_head");
                    $('#anchor' + anchor_num).scrollTo().animate({opacity: 0}, 'slow').animate({opacity: 1}, 'normal');
                    $("#anchor_num").val(anchor_num + 1);

                    if (parseInt($('.highlight2').length) <= anchor_num) {
                        $("#anchor_num").val(1);
                    }
                }
            } else {
                $(".law_highlight span.count").empty();
                $("#high_tmp").val("");
            }
        }
    });
    
    var $lawcontent = $('.main-huuliin-content');
    
    if (typeof lawlistcon !== 'undefined') {
        var $ind = 1;
        $.each(lawlistcon, function (index, row) {
            $lawcontent.find('.law_content').highlight(row.TITLE);
            console.log($('.main-huuliin-content').find('#active-tab-99'));
            $lawcontent.find('.highlight:not(.highligh-pa)').each(function (ii, rr) {
                var $selector = $(rr);
                $selector.attr('data-desc', $(rr).text().toLowerCase());
                $selector.attr("id", "anchor" + $ind);
                $selector.attr("data-index", $ind);

                if ($selector.find('i.highlighed').length < 1) {
                    $selector.append('<i class="fa fa-link highlighed" data-id="' + row.TRG_RECORD_ID + '"></i>').promise().done(function () {
                        if (typeof row['DTL'] !== 'undefined' && row['DTL']) {
                            var $pselector = $selector.closest('section');
                            $.each(row['DTL'], function (sindex, srow) {
                                $pselector.highlight1(' ' + srow.PNUMBER + ' ', srow.ID);
                                $pselector.highlight1(' ' + srow.PNUMBER + ',', srow.ID);
                            });
                        }
                    });
                }

                $ind++;
            });
        });

    }

});

$('body').on('click', '.up-tmp', function () {
    if ($("#high_tmp").val() == $('#high_val').val()) {
        $("#anchor_num").val($("#anchor_num").val() - 1);
        var anchor_num = parseInt($("#anchor_num").val());
        $('#anchor' + anchor_num).closest('.msg_body').show().prev('.msg_head').addClass("opened_head");
        $('#anchor' + anchor_num).scrollTo().animate({opacity: 0}, 'slow').animate({opacity: 1}, 'normal');

        if (parseInt($('.highlight2').length) <= anchor_num) {
            $("#anchor_num").val(1);
        }
    }
});

$('body').on('click', '.down-tmp', function () {
    if ($('input#high_val').val() != '') {
        if ($("#high_tmp").val() != $('input#high_val').val()) {
            $("#high_tmp").val($('input#high_val').val());
            $(".tab-pane").removeHighlight2();
            $(".tab-pane.active").removeHighlight2().highlight2($('input#high_val').val());

            $('.highlight2').each(function (index) {
                $(this).attr("id", "anchor" + (index + 1));
            });

            if (parseInt($('.highlight2').length) >= 1) {
                //$('#anchor1').closest('.msg_body').show().prev('.msg_head').addClass("opened_head");
                $('#anchor1').scrollTo().animate({ opacity: 0 }, 'slow').animate({ opacity: 1 }, 'normal');
                $("#anchor_num").val(2);
            }
            $(".law_highlight span.count").empty().append("Нийт олдсон: " + $('.highlight2').length);
        } else {
            var anchor_num = parseInt($("#anchor_num").val());

            $('#anchor' + anchor_num).closest('.msg_body').show().prev('.msg_head').addClass("opened_head");
            $('#anchor' + anchor_num).scrollTo().animate({ opacity: 0 }, 'slow').animate({ opacity: 1 }, 'normal');
            $("#anchor_num").val(anchor_num + 1);

            if (parseInt($('.highlight2').length) <= anchor_num) {
                $("#anchor_num").val(1);
            }
        }
    } else {
        $(".law_highlight span.count").empty();
        $("#high_tmp").val("");
    }
});

$('body').on('click', '.uk-offcanvas-close', function () {
    $('.haritsuulalt').hide();
    $('.shine-huuli-inner-container').removeAttr('style');
});

$('body').on('click', 'i.highlighed', function () {
    window.open(URL_APP + URL_LANG + '/detail?lawId=' + $(this).attr('data-id'), "_blank");
});

$('body').on('click', 'span.highligh-pa', function () {
    window.open(URL_APP + URL_LANG + '/pdtl/' + $(this).attr('id'), "_blank");
});

$('body').on('click', '.tw-fixed-action', function () {
    $(this).closest('.tw-fixed').hide();
});

$('body').on('change', '.header-filter', function () {
    
    var $this = $(this),
        $parent = $this.closest('.tab-pane');
    var dvId = $parent.attr('data-unid');
    var lawId = $parent.attr('data-lawid');

    $.ajax({
        type: 'post',
        dataType: 'json',
        url: URL_APP + URL_LANG + "/lawConnectedTypeData",
        data: {
            dvid: dvId,
            lawId: lawId,
            type: $this.attr('data-type'),
            title: $parent.find('input[name="title"]').val(),
            enacteddate: $parent.find('input[name="enacteddate"]').val(),
        },
        beforeSend: function () {
            Core.blockUI('section.tw-shine-huuli');
        },
        success: function (response) {
            $parent.empty().append(response.Html);
            Core.unblockUI();
        },
        error: function (jqXHR, exception) {
            Core.unblockUI();
            Core.showErrorMessage(jqXHR, exception);
        }
    });
    
});

document.onmouseup = document.onkeyup = document.onselectionchange = function () {
    var selectedText = window.getSelection().toString();
    $('a[data-text]').attr('data-text', selectedText);
};

function showCompare(keyid, element) {
    var $this = $(element);
    var $content = $this.closest('.main-huuliin-content').find('p.' + keyid);

    if (typeof $this.attr('data-status') !== 'undefined' && $this.attr('data-status') === 'open') {
        $this.attr('data-status', 'close');
        $content.hide();
        return;
    }

    $.ajax({
        type: 'post',
        dataType: 'json',
        url: URL_APP + URL_LANG + "/viewParagraph",
        data: {
            keyid: keyid,
            lawId: lawId
        },
        beforeSend: function () {
            Core.blockUI('section.tw-shine-huuli');
        },
        success: function (response) {

            $content.empty().append(response.Html).promise().done(function () {
                $content.show();
                $this.attr('data-status', 'open');
            });

            Core.unblockUI();

        },
        error: function (jqXHR, exception) {
            Core.unblockUI();
            Core.showErrorMessage(jqXHR, exception);
        }
    });
}

function showCompare1(keyid, element) {
    var $this = $(element);
    var $content = $this.closest('.nom-more-content').find('p.' + keyid + '_ADDIN');

    if (typeof $this.attr('data-status') !== 'undefined' && $this.attr('data-status') === 'open') {
        $this.attr('data-status', 'close');
        $content.hide();
        return;
    }

    $.ajax({
        type: 'post',
        dataType: 'json',
        url: URL_APP + URL_LANG + "/viewParagraph",
        data: {
            keyid: keyid,
            lawId: lawId
        },
        beforeSend: function () {
            Core.blockUI('section.tw-shine-huuli');
        },
        success: function (response) {

            $content.empty().append(response.Html).promise().done(function () {
                $content.show();
                $this.attr('data-status', 'open');
            });

            Core.unblockUI();
            return false;

        },
        error: function (jqXHR, exception) {
            Core.unblockUI();
            Core.showErrorMessage(jqXHR, exception);
        }
    });
}

function showActiveTab(index, element, dvid, annexId) {
    var $this = $(element),
            $parent = $this.closest('.tab-col'),
            $parentContent = $this.closest('.shine-huuli-inner-container');

    if (index == 1 || $parentContent.find('#active-tab-' + index).children().length > 0) {
        return;
    }

    $.ajax({
        type: 'post',
        dataType: 'json',
        url: URL_APP + URL_LANG + "/lawConnectedTypeData",
        data: {
            dvid: dvid,
            type: index,
            lawId: lawId,
            annexId: annexId
        },
        beforeSend: function () {
            Core.blockUI('section.tw-shine-huuli');
        },
        success: function (response) {
            $parentContent.find('#active-tab-' + index).empty().append(response.Html);
            Core.unblockUI();
        },
        error: function (jqXHR, exception) {
            Core.unblockUI();
            Core.showErrorMessage(jqXHR, exception);
        }
    });
}

function showAdditionalLaw(lawId, $trgId, annexId) {
    if (typeof annexId !== 'undefined') {
        window.open(URL_APP + URL_LANG + '/detail?lawId=' + lawId + '&trgId=' + $trgId + '&annexId=' + annexId, "_blank");
    } else {
        window.open(URL_APP + URL_LANG + '/claw/' + lawId + '/' + $trgId, "_blank");
    }
    return;
    
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: URL_APP + URL_LANG + "/additionalLaw",
        data: {
            lawId: lawId
        },
        beforeSend: function () {
            Core.blockUI('section.tw-shine-huuli');
        },
        success: function (response) {
            $('#preview-law').find('.modal-body').empty().append(response.Html).promise().done(function () {
                UIkit.modal('#preview-law').show();
                
                setTimeout(() => {
                    /* var $ps = $('section[id="' + $trgId +'"]').offset().top - 350;
                    $('body #preview-law .modal-body').animate({
                        scrollTop: $ps
                    }, 2000);*/

                    $('section[id="'+ $trgId +'"]').attr('style', $('section[id="'+ $trgId +'"]').attr('style') + 'background:#2196f354;'); 
                }, 1000);
                
            });
            Core.unblockUI();

        },
        error: function (jqXHR, exception) {
            Core.unblockUI();
            Core.showErrorMessage(jqXHR, exception);
        }
    });
}

function listenlaw(element) {
    $('body').find('.tw-fixed').hide();
    var $this = $(element),
            $parent = $this.closest('.listen'),
            text = $this.attr('data-text');

    $.ajax({
        type: 'post',
        url: URL_APP + URL_LANG + '/apichimege',
        data: {text: text},
        dataType: 'json',
        beforeSend: function () {
            Core.blockUI({
                message: 'Боловсруулж байна, түр хүлээнэ үү...',
                boxed: true
            });
        },
        success: function (response) {
            $('body').find('.tw-fixed .tw-fixed-content').empty().append(response.file).promise().done(function () {
                $('body').find('.tw-fixed').show();
                Core.unblockUI();
            });

        },
        error: function (jqXHR, exception) {
            Core.unblockUI();
            Core.showErrorMessage(jqXHR, exception);
        }
    });
}

function changeradio(element) {
    var $this = $(element);

    if ($this.attr('data-status') === 'checked') {
        $this.attr('data-status', 'unchecked');
        $this.prop("checked", false);
        $('body').find('.main-huuliin-content').find('[id="' + $this.attr('data-id') + '"]').hide();
        $('body').find('.main-huuliin-content').find('span#' + $this.attr('data-id')).hide();
    } else {
        $('body').find('.main-huuliin-content').find('[id="' + $this.attr('data-id') + '"]').show();
        $('body').find('.main-huuliin-content').find('span#' + $this.attr('data-id')).show();
        $this.attr('data-status', 'checked');
    }
}

function changeradioAll(element) {
    var $this = $(element);
    if ($this.attr('data-status') === 'checked') {
        $this.attr('data-status', 'unchecked');
        $this.prop("checked", false);

        $('form.sanal-form input[type="radio"]').each(function (index, row) {
            $(row).prop("checked", false);
            $(row).attr('data-status', 'unchecked');
            $('[id="' + $(row).attr('data-id') + '"]').hide();
            $('span[id="' + $(row).attr('data-id') + '"]').hide();
        });
    } else {
        $this.attr('data-status', 'checked');
        $('form.sanal-form input[type="radio"]').each(function (index, row) {
            $(row).prop("checked", true);
            $(row).attr('data-status', 'checked');
            $('[id="' + $(row).attr('data-id') + '"]').show();
            $('span[id="' + $(row).attr('data-id') + '"]').show();
            $('div[data-id="' + $(row).attr('data-id') + '"]').find('ul[uk-accordion] > li').each(function (index, row) {
                $(row).addClass('uk-open');
                $(row).find('.uk-accordion-content:eq(0)').removeAttr('hidden');
            });

        });
    }
}

function downloadlaw(type, $llId) {
    if (type == '1') {
        $.fileDownload(URL_APP + URL_LANG + '/pdfExport', {
            httpMethod: 'POST',
            data: {
                'fileid': typeof $llId !== 'undefined' ? $llId : lawId,
                'orientation': 'portrait',
                'size': 'a4',
                'top': '1cm',
                'left': '1.5cm',
                'bottom': '1cm',
                'right': '0.5cm',
                'width': '',
                'height': '',
                'fontfamily': 'Arial, Helvetica, sans-serif',
            }
        }).done(function () {
            Core.unblockUI();
        }).fail(function () {
            alert("File download failed!");
            Core.unblockUI();
        });
        return false;
    } else {
        $.fileDownload(URL_APP + URL_LANG + '/wordExport', {
            httpMethod: 'POST',
            data: {
                'fileid': typeof $llId !== 'undefined' ? $llId : lawId,
                'orientation': 'portrait',
                'size': 'a4',
                'top': '1cm',
                'left': '1.5cm',
                'bottom': '1cm',
                'right': '0.5cm',
                'width': '',
                'height': '',
                'fontfamily': 'Arial, Helvetica, sans-serif',
            }
        }).done(function () {
            Core.unblockUI();
        }).fail(function () {
            alert("File download failed!");
            Core.unblockUI();
        });
        return false;
    }
}

function printlaw(element) {
    var $html = $(element).closest('.tab-pane').find('.maincontenter:eq(0)').html();
    
    $html = $html.replaceAll('font-size', 'font-family: Arial !important; fsize');
    $html = $html.replaceAll('padding: 20mm 15mm 30mm 30mm', '');
    $html = $html.replaceAll('font-family', 'ffamily');
    $('.print_preview').empty().append($html).promise().done(function () {

        $('.print_preview').find('.print-zuil').remove();
        $('.print_preview').find('.printpage').remove();
        /*$('.print_preview').find('div[style="display: none;"]').remove();
         $('.print_preview').find('.uk-drop').remove();
         $('.print_preview').find('.uk-position-top-right').remove();
         $('.print_preview').find('.dropped').remove();
         $('.print_preview').find('.uk-accordion-content').removeAttr('hidden');
         
         $('.print_preview').find('div[data-id]').find('ul[uk-accordion] > li').each(function (index, row) {
         $(row).addClass('uk-open');
         $(row).find('.uk-accordion-content:eq(0)').removeAttr('hidden');
         });*/
    });

    var css = '';
    css += ' a {';
    css += ' text-decoration: none;';
    css += '} ';
    css += ' ul {';
    css += 'padding: 0;';
    css += 'list-style: none;';
    css += '} ';
    css += '.nom-title {';
    css += 'font-size: 12px;';
    css += 'line-height: 22px;';
    css += 'text-align: center;';
    css += 'text-transform: uppercase;';
    css += 'color: #2E3B52;';
    css += 'max-height: 28px;';
    css += 'overflow: hidden;';
    css += 'margin-top: 21px;';
    css += '} ';
    css += '.nom-header-top .title {';
    css += 'font-weight: bold;';
    css += 'font-size: 14px;';
    css += 'line-height: 22px;';
    css += 'color: #2E3B52;';
    css += 'text-transform: uppercase;';
    css += 'margin-bottom: 0;';
    css += 'text-align: center;';
    css += '}';
    css += '.nom-header-top span {';
    css += 'font-weight: 700;';
    css += 'font-size: 11px;';
    css += 'line-height: 22px;';
    css += 'color: #000000;';
    css += '}';
    css += '.nom-header-top span {';
    css += 'font-weight: 700;';
    css += 'font-size: 11px;';
    css += 'line-height: 22px;';
    css += 'color: #000000;';
    css += '}';
    css += '.nom-more-content .uk-accordion-title {';
    css += 'font-weight: 700;';
    css += 'font-size: 12px;';
    css += 'line-height: 14px;';
    css += 'color: #2E3B52;';
    css += '}';
    css += '.nom-more-content .uk-accordion-content p {';
    css += 'font-size: 12px;';
    css += 'line-height: 22px;';
    css += 'color: rgba(46, 59, 82, 0.8);';
    css += 'margin-bottom: 0;';
    css += '}';
    css += '.nom-bottom-author {';
    css += 'max-width: 545px;';
    css += 'margin-left: auto;';
    css += 'margin-top: 70px;';
    css += '}';
    css += '.nom-bottom-author h6 {';
    css += 'font-weight: 700;';
    css += 'font-size: 14px;';
    css += 'line-height: 22px;';
    css += 'color: #000000;';
    css += '}';
    css += '.uk-flex-between {';
    css += 'justify-content: space-between;';
    css += '}';
    css += '.uk-text-center {';
    css += 'text-align: center;';
    css += '}';
    css += '.uk-flex {';
    css += 'display: flex;';
    css += '}';
    css += 'section, p {';
    css += 'line-height: 22px;';
    css += 'font-size: 14px !important;';
    css += 'font-family: Arial, Helvetica, sans-serif !important;';
    css += '}';
    css += '.pull-left {';
    css += 'float:left;';
    css += '}';
    css += '.w-100 {';
    css += 'width: 100%;';
    css += '}';
    $('.print_preview').printThis({
        debug: false,
        importCSS: false,
        printContainer: false,
        dataCSS: css,
        removeInline: false
    });
}

function printZuil(id) {
    var $html = $('div#' + id).html();
    $('[data-parentid="' + id + '"]').each(function (index, row) {
        $html += $(row).html();
    });
    
    $html = $html.replaceAll('font-size', 'font-family: Arial !important; fsize');
    $html = $html.replaceAll('padding: 20mm 15mm 30mm 30mm', '');
    $html = $html.replaceAll('font-family', 'ffamily');
    $('.print_preview').empty().append($html).promise().done(function () {
    });

    var css = '';
    css += ' a {';
    css += ' text-decoration: none;';
    css += '} ';
    css += ' ul {';
    css += 'padding: 0;';
    css += 'list-style: none;';
    css += '} ';
    css += '.nom-title {';
    css += 'font-size: 12px;';
    css += 'line-height: 22px;';
    css += 'text-align: center;';
    css += 'text-transform: uppercase;';
    css += 'color: #2E3B52;';
    css += 'max-height: 28px;';
    css += 'overflow: hidden;';
    css += 'margin-top: 21px;';
    css += '} ';
    css += '.nom-header-top .title {';
    css += 'font-weight: bold;';
    css += 'font-size: 14px;';
    css += 'line-height: 22px;';
    css += 'color: #2E3B52;';
    css += 'text-transform: uppercase;';
    css += 'margin-bottom: 0;';
    css += 'text-align: center;';
    css += '}';
    css += '.nom-header-top span {';
    css += 'font-weight: 700;';
    css += 'font-size: 11px;';
    css += 'line-height: 22px;';
    css += 'color: #000000;';
    css += '}';
    css += '.nom-header-top span {';
    css += 'font-weight: 700;';
    css += 'font-size: 11px;';
    css += 'line-height: 22px;';
    css += 'color: #000000;';
    css += '}';
    css += '.nom-more-content .uk-accordion-title {';
    css += 'font-weight: 700;';
    css += 'font-size: 12px;';
    css += 'line-height: 14px;';
    css += 'color: #2E3B52;';
    css += '}';
    css += '.nom-more-content .uk-accordion-content p {';
    css += 'font-size: 12px;';
    css += 'line-height: 22px;';
    css += 'color: rgba(46, 59, 82, 0.8);';
    css += 'margin-bottom: 0;';
    css += '}';
    css += '.nom-bottom-author {';
    css += 'max-width: 545px;';
    css += 'margin-left: auto;';
    css += 'margin-top: 70px;';
    css += '}';
    css += '.nom-bottom-author h6 {';
    css += 'font-weight: 700;';
    css += 'font-size: 14px;';
    css += 'line-height: 22px;';
    css += 'color: #000000;';
    css += '}';
    css += '.uk-flex-between {';
    css += 'justify-content: space-between;';
    css += '}';
    css += '.uk-text-center {';
    css += 'text-align: center;';
    css += '}';
    css += '.uk-flex {';
    css += 'display: flex;';
    css += '}';
    css += 'section, p {';
    css += 'line-height: 22px;';
    css += 'font-size: 14px !important;';
    css += 'font-family: Arial, Helvetica, sans-serif !important;';
    css += '}';
    css += '.pull-left {';
    css += 'float:left;';
    css += '}';
    css += '.w-100 {';
    css += 'width: 100%;';
    css += '}';
    $('.print_preview').printThis({
        debug: false,
        importCSS: false,
        printContainer: false,
        dataCSS: css,
        removeInline: false
    });
}

function changeHighLight() {
    if ($("#high_tmp").val() != $('#high_val').val()) {
        $("#high_tmp").val($('#high_val').val());
        $(".tab-pane").removeHighlight2();
        $(".tab-pane.active").removeHighlight2().highlight2($('#high_val').val());

        $('.highlight2').each(function (index) {
            $(this).attr("id", "anchor" + (index + 1));
        });

        if (parseInt($('.highlight2').length) >= 1) {
            //$('#anchor1').closest('.msg_body').show().prev('.msg_head').addClass("opened_head");
            $('#anchor1').scrollTo().animate({opacity: 0}, 'slow').animate({opacity: 1}, 'normal');
            $("#anchor_num").val(2);
        }
        $(".law_highlight span.count").empty().append("Нийт олдсон: " + $('.highlight2').length);
    } else {
        var anchor_num = parseInt($("#anchor_num").val());

        $('#anchor' + anchor_num).closest('.msg_body').show().prev('.msg_head').addClass("opened_head");
        $('#anchor' + anchor_num).scrollTo().animate({opacity: 0}, 'slow').animate({opacity: 1}, 'normal');
        $("#anchor_num").val(anchor_num + 1);

        if (parseInt($('.highlight2').length) <= anchor_num) {
            $("#anchor_num").val(1);
        }
    }
}