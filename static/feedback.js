
jQuery(document).ready(function () {

    $('body').find('.feedback-code-refresh').click(function () {
        var c_currentTime = new Date();
        var c_miliseconds = c_currentTime.getTime();
        $(this).closest('div').find("#captcha").attr('src', 'api/captcha?x=' + c_miliseconds);
    });

    $('body').find('.feedback_left:not(.today_fixed_law)').click(function () {
        if ($('body').find('.feedback_rContainer:not(.law_sugg_rContainer)').hasClass('feedback-collapsed')) {
            $('body').find('.feedback_rContainer:not(.law_sugg_rContainer)').removeClass('feedback-collapsed').addClass('feedback-expanded');
            $('body').find('.feedback-hide-place:not(.law_sugg-hide-place)').removeClass('hide');
            $('body').find('.law_sugg_rContainer:not(.d-none)').hide();
        } else {
            $('body').find('.feedback_rContainer:not(.law_sugg_rContainer)').addClass('feedback-collapsed').removeClass('feedback-expanded');
            $('body').find('.feedback-hide-place:not(.law_sugg-hide-place)').addClass('hide');
            $('body').find('.law_sugg_rContainer:not(.d-none)').show();
        }
    });

    $('body').find('.today_fixed_law').click(function () {
        if ($('body').find('.law_sugg_rContainer').hasClass('feedback-collapsed')) {
            $('body').find('.law_sugg_rContainer').removeClass('feedback-collapsed').addClass('feedback-expanded');
            $('body').find('.law_sugg-hide-place').removeClass('hide');
        } else {
            $('body').find('.law_sugg_rContainer').addClass('feedback-collapsed').removeClass('feedback-expanded');
            $('body').find('.law_sugg-hide-place').addClass('hide');
        }
    });

    $('body').find('.feedback-add').click(function () {
        if ($('body').find('.feedback_rContainer:not(.law_sugg_rContainer)').hasClass('feedback-collapsed')) {
            $('body').find('.feedback_rContainer:not(.law_sugg_rContainer)').removeClass('feedback-collapsed').addClass('feedback-expanded');
            $('body').find('.feedback-hide-place:not(.law_sugg-hide-place)').removeClass('hide');
        } else {
            $('body').find('.feedback_rContainer:not(.law_sugg_rContainer)').addClass('feedback-collapsed').removeClass('feedback-expanded');
            $('body').find('.feedback-hide-place:not(.law_sugg-hide-place)').addClass('hide');
        }
    });

    $('body').on('click', '.feedback_cancel', function () {
        $('body').find('.feedback-hide-place:not(.law_sugg-hide-place)').addClass('hide');
        var $parent = $('body').find('.feedback_rContainer:not(.law_sugg_rContainer)');
        $parent.removeClass('feedback-expanded');
        $parent.addClass('feedback-collapsed');
        $('body').find('.law_sugg_rContainer:not(.d-none)').show();
    });

    $('body').find('#feedbackType').change(function () {
        var value = $(this).find('option:selected').data('code');
        if (value == 'ACT') {
            $('body').find('div.feedback-url-div').show();
            $('body').find('div.feedback-email-div').show();
            $('body').find('div.feedback-title-div').hide();
            $('body').find('div.feedback-title-div').find('#feedback-title').val("");
        } else if (value == 'SYSTEM') {
            $('body').find('div.feedback-url-div').show();
            $('body').find('div.feedback-email-div').hide();
            $('body').find('div.feedback-email-div').find('#feedback-email').val("");
            $('body').find('div.feedback-title-div').hide();
            $('body').find('div.feedback-title-div').find('#feedback-title').val("");
        } else if (value == 'OTHER') {
            $('body').find('div.feedback-url-div').hide();
            $('body').find('div.feedback-url-div').find('#feedback-url').val("");
            $('body').find('div.feedback-email-div').hide();
            $('body').find('div.feedback-email-div').find('#feedback-email').val("");
            $('body').find('div.feedback-title-div').show();
        }
    });

    $('.feedback_rContainer:not(.law_sugg_rContainer)').find('button.feedback_send').click(function () {
        $.ajax({
            url: URL_APP + URL_LANG + '/feedBackAnonymous',
            type: 'post',
            dataType: 'json',
            data: {type: $("#feedbackType").val(), email: $("#feedback-email").val(), url: $("#feedback-url").val(), title: $("#feedback-title").val(), body: $("#feedback-content").val(), security_code: $('#feedback-security_code').val()},
            beforeSend: function () {
                blockContent('.feedback_rContainer:not(.law_sugg_rContainer)');
                PNotify.removeAll();
            },
            success: function (response) {
                if (response.status == 'success') {
                    new PNotify({
                        title: 'Амжилттай',
                        text: 'Амжилттай бүртгэгдлээ.',
                        type: 'success',
                        sticker: false
                    });
                    $("#feedback-email").val('');
                    $("#feedback-content").val('');
                    $("#feedback-title").val('');
                    $("#feedback-url").val('');
                    $("#feedback-security_code").val('');
                    var c_currentTime = new Date();
                    var c_miliseconds = c_currentTime.getTime();
                    $('body').find('.feedback_rContainer:not(.law_sugg_rContainer)').find("#captcha").attr('src', 'api/captcha?x=' + c_miliseconds);
                } else if (response.status == 'error') {
                    new PNotify({
                        title: 'Алдаа',
                        text: response.text,
                        type: 'error',
                        sticker: false
                    });
                }
                Core.unblockUI('.feedback_rContainer:not(.law_sugg_rContainer)');

            },
            error: function (jqXHR, exception) {
                Core.unblockUI('.feedback_rContainer:not(.law_sugg_rContainer)');
                Core.showErrorMessage(jqXHR, exception);
            }
        });
    });

    function blockContent(mainSelector) {
        $(mainSelector).block({
            message: '<i class="icon-spinner4 spinner"></i>',
            centerX: 0,
            centerY: 0,
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                width: 16,
                top: '15px',
                left: '',
                right: '15px',
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
    }

    document.onclick = function (e) {
        if ($(e.target).hasClass('feedback-hide-place')) {
            $('body').find('.feedback-hide-place').addClass('hide');
            var $parent = $('body').find('.feedback_rContainer');
            $parent.removeClass('feedback-expanded');
            $parent.addClass('feedback-collapsed');
        }
    };

    $("div#home-popup").addClass("uk-flex uk-open");

    $("div#home-popup").find('.modal-close-btn').click(function () {
        var isShowAgain = $("div#home-popup").find('.modal-dont-showagain:checked').val();
        if (isShowAgain) {
            $.ajax({
                url: URL_APP + URL_LANG + '/dontshowpopup',
                type: 'post',
                dataType: 'json',
                complete: function () {
                    $("div#home-popup").removeClass("uk-flex uk-open");
                }
            });
        } else {
            $("div#home-popup").removeClass("uk-flex uk-open");
        }
    });
    
});


window.onscroll = function () {
    scrollFunction();
};

function scrollFunction() {
    if (document.body.scrollTop > $(window).height() - 100 || document.documentElement.scrollTop > $(window).height() - 100) {
        $("#topBtn").show();
    } else {
        $("#topBtn").hide();
    }
}

function scroll2top() {
    $('html, body').animate({
        scrollTop: 0
    }, 800);
}

function twFixContentHide(element) {
    $('.tw-fixed').closest('.tw-fixed').hide();
    $('.law_sugg_rContainer').hide();
    $('.law_sugg_rContainer').removeClass('d-none');
    $('.law_sugg_rContainer').show(1000)
}